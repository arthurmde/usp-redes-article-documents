\documentclass[12pt]{article}

\usepackage{sbc-template}
\usepackage{graphicx,url}
\usepackage[brazil]{babel}
\usepackage[utf8]{inputenc}
\usepackage{float}
\usepackage{setspace}

\usepackage{tabularx}
\usepackage{cite}

\begin{document}
\sloppy
\title{A Collaboration System to Recommend Cyber Security Alerts for Network Administrators Based on Unstructured Data: From Model to Code}

\author{Arthur de Moura Del Esposte\inst{1}}

\address{Instituto de Matemática e Estatística -- Universidade de São Paulo (USP)\\
  Rua do Matão, 1010 -- 05508-090 -- Cidade Universitária -- São Paulo -- SP -- Brasil
  \email{esposte@ime.usp.br}
}

\maketitle
\begin{abstract}

With the exponential growth of the Internet's users, services and dependence
in most human fields, cyber security issues are becoming increasingly important,
since the number of cyber threats and known vulnerabilities have also grown
rapidly. Hence, a large amount of cyber security alerts is created and shared in
different medias as unstructured data. Despite the large number of alerts,
network administrators hardly ever find all important information or read most
cyber security alerts of interest. Thus, Recommender Systems methods and
techniques can be properly used to filter cyber security alerts based on
network administrators ratings and preferences. Therefore, this work aims to
create a collaboration system to recommend cyber security alerts for network
administrators through a design and offline evaluation of a recommendation
model.

\end{abstract}

\textbf{Keywords:} recommender system, network administrators, cyber security,
text mining, offline evaluation

\section{Introduction} \label{sec:intro}

The Internet has grown significantly in recent years, especially in number of
users, offered services and data trasnfer. Thereby, most human activity fields,
such as education, health, business, economy and social interactions
have envolved with the expansion of the Internet.

More and more people, enterprises, governments and critical infrastructures
depend on information and communication technologies (ICT) and the
infrastructure that make up cyberspace. Cyber security and resilience of
critical services and critical service chains are therefore seen as increasingly
important governance topics and major challenges for today’s societies \cite{bruce2005}.

In order to address these challenges, it's necessary to increase the cyber
security to ensure the confidentiality, availability and integrity in all
layers related to IT, from physical components to organizational procedures.
However, the risk to information and computer assets comes from a broad
spectrum of threats with a broad range of capabilities that come into existence
from the presence of vulnerabilities \cite{gchq2015}.

Formally, a vulnerability can be defined as an instance of failure in the
specification, development or software configuration so that its execution can
violate security polices, implicit or explicit \cite{krsul1998}. Vulnerabilities
can be maliciously exploited to allow unauthorized access, privileges changes
and denial of service. Although the IT is composed by many physical, logical and
organizational components, most of the exploitable vulnerabilities is present in
softwares. ICAT/NIST
\footnote{ ICAT was a search engine for vulnerabilities, developed by
NIST(\emph{National Institute of Standards and Technology}), cataloged in
the CVE standard. The ICAT was placed by NVD (\emph{National Vulnerability
Database}) witch in additional to having the same search engine, it is a US
government repository that stores various informations about software
vulnerabilities like nomenclatures, metrics, checklists, etc.}
2005 data already evidenced that 80\% of remote exploitable
vulnerabilities are related to poor coding programs\cite{duarte2005}. Besides,
according to the Internet Security Threat Report \cite{istr2014}, in 2013 77\%
of websites cointained vulnerabilities.

The more new cyber security issues arise, the more network administrators must
update and improve their knowledge about those issues in order implement
preventive actions on computer networks and services. As long as many new
attacks occur and new discovered vulnerabilities increases, many alerts
and news are released in all sorts of media. Specialized Websites and e-mail
lists are examples of traditional medias used to warn about security threats,
though they are not always effective in quickly publish recent threats
\cite{frei2006}. As discursed by Santos and collaborators\cite{santos2013},
alternative decentralized medias, such as social networks, provide a rich
and heterogeneous data source that can be used as means of spreading security
notifications. This speech is reinforced by Surjandari and colleagues
\cite{surjandari2015} who claim that social networks are valuable platforms for
tracking and analyzing information, once people post real time messages about
their opinions on a variety of topics and disseminate information quickly and
collaboratively.

Despite the large number of news and warnings about cyber security generated
over the Internet every day, a network administrator not always get relevant or
information of interest. In this sense, the large number of data source can
become a problem once it is a hard task to find or filter information of
interest manually. Historically, a natural way to do this task is rely on
recommendations from people of same interest profile or on the advice of
experts. Ekstrand et al\cite{ekstrand2011} expose that computer-based systems
provide the opportunity to expand the set of people from whom users can obtain
recommendations. They also enable us to mine users’ history and stated
preferences for patterns that neither they nor their acquaintances identify,
potentially providing a more finely-tuned selection experience. The set of
computer systems that provide those services are called Recommender Systems.
Ricci and colleagues, in their book Recommender Systems Handbook
\cite{ricci2011}, define Recommender Systems as softwares tools and techniques
witch providing suggestions for items to be use to a user.

In the proposal paper we propose the development of a collaboration system
to recommend cyber security alerts for network administrators. Beyond the
techonologycal challenges, there is a set of other issues that have to be
addressed in the present work. Therefore, this paper also will describe:
techniques and algorithms used to text mining in order to extract useful cyber
security information available as unstructured data; how to build a
recommendation model to cyber security news based on system administrators
requirements; how to evaluate the proposed model;


\section{Goals and Objectives}\label{sec:goals}

With the purpose of build a collaboration system to recommend cyber security
alerts for network administrators from the text mining of unstructured data, we
hope to accomplish a set of scientific contributions listed bellow:

\begin{itemize}
\item Purpose a recommendation model to correlate network administrators'
interests based on mixing the Recommender Systems' Collaboration Filtering,
Community-based and Content-based techniques.

\item Apply social networks concepts to facilitate collaboration between
network administrators

\item Apply machine learning mechanisms and natural language processing
techniques proposed in the literature to extract cyber security information from
unstructured data

\item Conduct an offline experiment to evaluate the proposed model
\end{itemize}

Furthermore, we want to build and release an initial version of a System witch
implements the proposed model as a technological contribution. In this text,
we reference to this initial system version as a \emph{Functional Prototype}.

\section{Methods}\label{sec:methods}

This section describes the core methods used to achieve the goals and objectives
described in the section \ref{sec:goals}. Before describe all methods, we set up
a ensemble of assumptions and limitations that will be used to guide the work
and have not been defined by applying a specific methodology:

\begin{itemize}
  \item For the Functional Prototype, we will use up to two different fonts of
cyber security unstructured data:

  \begin{itemize}
    \item From a specialized news blog or RSS feed.
    \item From messages in a social network.
  \end{itemize}

  \item The System must apply social network concepts in order to produce social
  media and collaborations

  \item The Recommendation model is destinated to general users with network
  administrator profile. We will consider the following basics characterisics
  to define this profile:
  \begin{itemize}
    \item Technical knowledge about computers networks
    \item Technical knowledge about operational systems
    \item People technically interested in cyber security
  \end{itemize}
\end{itemize}

\subsection{Requirements and Context}
\label{requirements}

Following the main definitions of entities related to a Recommender System
\cite{ricci2011} , the first step to design the recommendation model is to
identify some initial information about the user who will compose the model,
the main characterisics about cyber security alerts witch will match to the
model items and the possibilities of interactions between users and items.

For this purpose we have already designed a qualitative survey to be applied for
network administrators professionals. This survey was designed with four parts:

\begin{itemize}
  \item \emph{Profile Survey}: to identify some gereral characteristics of
  potential users.
  \item \emph{Cyber Security Survey}: to identify how much the users know about
  cyber security issues and whether they use any communication channel to be up
  to date about those issues.
  \item \emph{Interest Assessment}: to identify requirements that must be
  considered into the Recommendation Model conception. It also helps to get
  what are the main elements that must compose the Recommendation Model item by
  asking the users what are the most important elements of a cyber security
  alert. Finally, this section also provide questions that will be used to
  estabilish some interactions between users and items.
  \item \emph{Use Cases}: this section was designed to simulate some use cases
  of an hypothetical Recommender System to evidence in more details potential
  users' interactions.
\end{itemize}

We will apply this survey to computer network professionals throuth
ESurv\footnote{http://esurv.org/}, an online web-based free tool to create
surveys. We hope to have at least 8 answers to get enougth data to guide the
Recommender Model development.


\subsection{Text Mining}
\label{text-mining}

Once we have collected and analized the survey results, it's necessary to build
a data structure to represent the model item. All data sources we defined
are mostly composed by unstructured textual data. In this sense we will apply
some Text Mining techniques proposed in the literature, witch are used to
extracting unstructured information from a set of textual
data(\cite{feldman2007}\cite{santos2013}\cite{surjandari2015}\cite{bonchi2011}),
and map the resulting information to the defined data structure.

The same data structure will be used for both choosen data sources. It also must
contemplate attributes that can be used to rate the items.

\subsection{Recommendation Model Development}
\label{model}

The main objective of the proposed paper is to build a Recommendation Model to
recommend cyber security alerts, collected from external data sources, to
network administrators. For this model, there are few essential requirements:

\begin{enumerate}
  \item The model always have to recommend some items even if the user has never
  informed their preferences and even if is that user first interaction with the
  model.
  \item The model must consider user preferences to recommend items.
\end{enumerate}

The prerequisites to build the proposed Recommendation Model we firstly need
to properly define the User and the Item elements. Both elements can be defined
as a set of data structures that represent the real world related entity.

The Item data structure definition was described in the subsection
\ref{text-mining}. To define the User data structure we will use the collected
data from our survey. In order to meet all essential requirements the proposal
model will mix three sorts of recommendation methods.

In order to meet the first essential described requirement
we can use a Community-based method. This type of models recommends
items based on the preferences of the users friends or peers \cite{ricci2011}.
Since the model is proposed for a specific user profile, we can consider the
gerneral ratings for this purpose.

With the purpose to address the second essential described requirement it is necessary
to define attributes that can be informed by users to set their preferences,
such as used operational systems, used softwares and interests. In this sense,
Collaborative Filter method may also be used to consider others users with
similar preferences items to recommend. Collaborative filtering is the most
widely used type of recommendation. It works assuming that two peers who had
similar preferences in the past will also have similar preferences in the
future\cite{boratto2011}. In this sense, we also will use the survey results
to define some attributes that can be informed by users to set their
preferences, such as used operational systems, used softwares and interests.

Finally, we must also consider Content-based methods to improve the model.
Content-based models learns to recommend items that are similar to the
ones that the user liked in the past. The similarity of items is calculated
based on the features associated with the compared items\cite{ricci2011}.

\subsection{Funciontal Prototype Development}
\label{model}

The main technological contribution is the development of a functional prototype
that implements the designed recommendation model. We will not adopt a specific
software development methodology to build the referenced prototype, although
some of the best and well recognized software engineering practices will be used
(i.e: Interactive and incremental development and Automated testing).

\section{Results Analysis}\label{sec:results}

As described in \cite{ricci2011} to evaluate a recommendation model, we must
hence identify the set of properties that may influence the success of a
recommender system or model in the context of a specific application.
Then, we can evaluate how the system preforms on these relevant properties. In
the proposal model, the Offline Experiment approach will be used to evaluate
the properties of the built model. In order to performe Offline Experiments it
is required to perform the following steps:

\begin{enumerate}
  \item Define the data sets
  \item Simulate user behaviour
\end{enumerate}

In this paper, we want to evaluate the \emph{Prediction Accuracy} of the
recommendation model, using the method described by Cremonesi and collaborators
\cite{cremonesi2010}.


\section{Work Plan and Schedule}\label{sec:plan}

The plan showed bellow takes into account the development of the items proposed
for the article over two months. To enhance visualization and understanding of
the plan, the related activities are listed per week.

\begin{itemize}
  \item \emph{Week 1}:
    \begin{itemize}
      \item Apply designed survey;
      \item Choose the two unstructured data sources;
      \item Study data sources and choose properly text mining techniques
    \end{itemize}
  \item \emph{Week 2}:
    \begin{itemize}
      \item Analyze survey results;
      \item Model: Design model item data structure;
      \item Model: Design model user data structure;
    \end{itemize}
  \item \emph{Week 3}:
    \begin{itemize}
      \item Implement the choseen text mining algorithms and create model items
      with the information;
      \item Prototype: implement user and item model/views/controllers;
      \item Prototype: implement user authentication;
    \end{itemize}
  \item \emph{Week 4}:
    \begin{itemize}
      \item Prototype: automate data extraction and item creation;
      \item Model: choose and design the properly recommendation algorithms;
      \item Document: document all decisions made so far;
    \end{itemize}
  \item \emph{Week 5}:
    \begin{itemize}
      \item Evaluation: define the experiment protocol;
      \item Evaluation: collect or create the data for the experimentation;
      \item Prototype: develope item ratings mechanisms;
      \item Document: document all decisions and results so far;
    \end{itemize}
  \item \emph{Week 6}:
    \begin{itemize}
      \item Evaluation: simulate user behaviour;
      \item Prototype: develope the recommendation model transactions;
      \item Document: document all decisions and results so far;
    \end{itemize}
  \item \emph{Week 7}:
    \begin{itemize}
      \item Evaluation: analyse experimental data;
      \item Prototype: develope the recommendation model transactions;
      \item Document: document all decisions and results so far;
    \end{itemize}
  \item \emph{Week 8}:
    \begin{itemize}
      \item Prototype: develope the recommendation model transactions;
      \item Prototype: refine implementations details;
      \item Document: document all decisions and results so far;
      \item Document: finish the article final version;
    \end{itemize}
\end{itemize}

\bibliographystyle{sbc}
\bibliography{proposal}

\end{document}
