# Computer Network Programming Article - IME/USP

This repository is related to the development of a new Recommander System for
network administrators and a Article. This work started in an USP's Computer
Science Master subject: Network Programming. 

It contains two folders:

* **article**: all files related to the Article.
* **presentations**: all presentantios made during the subject.

## Authors

1. Arthur de Moura Del Esposte
2. Rodrigo Campiolo
3. Prof. Daniel
4. Prof. Fabio Kon